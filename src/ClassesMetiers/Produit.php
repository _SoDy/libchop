<?php

namespace Eshop\ClassesMetiers ;

use Eshop\Db\DBA;


class Produit {

    private $idProduit;
    private $libelle;
    private $description;
    private $prix;
    private $image;
    private $categorie;

    public function setCategorie(Categorie $categorie = null) {
        $this->categorie = $categorie;
        if ($categorie != null) {
            $categorie->addProduit($this);
        }

    }

    public function getCategorie() {
        return $this->categorie;
    }

    public function getIdProduit() {
        return $this->idProduit;
    }

    public function getLibelle() {
        return $this->libelle;
    }

    public function getDescription() {
        return $this->description;
    }

    public function getPrix() {
        return $this->prix;
    }

    public function getImage() {
        return $this->image;
    }

    public function setLibelle($libelle): void {
        $this->libelle = $libelle;
    }

    public function setDescription($description): void {
        $this->description = $description;
    }

    public function setPrix($prix): void {
        $this->prix = $prix;
    }

    public function setImage($image): void {
        $this->image = $image;
    }

    private static $select = "select * from produit";
    private static $selectById = "select * from produit where idProduit = :idProduit";
    private static $update = "update produit set libelle=:libelle, description=:description,image=:image,prix=:prix,
idCategorie=:idCategorie where idProduit=:idProduit";
    private static $insert = "insert into produit (libelle,description,image,prix,idCategorie) values (:libelle,:description,:image,:prix,:idCategorie)";
    private static $delete = "delete from produit where idProduit = :idProduit";
    private static $selectByIdCategorie = "select * from produit where idCategorie=:idCategorie";

    private static function arrayToProduit(Array $array, Categorie $categorie = null) {
        $p = new Produit();
        $p->idProduit = $array["idProduit"];
        $p->libelle = $array["libelle"];
        $p->description = $array["description"];
        $p->image = $array["image"];
        $p->prix = $array["prix"];
        if ($categorie == null) {
            if ($array["idCategorie"] != null) {
                $p->categorie = Categorie::fetch($array["idCategorie"]);
            } else {
                $p->categorie = null;
            }
        } else {
            $p->categorie = $categorie;
        }
        return $p;
    }

    public static function fetchAllByCategorie(Categorie $categorie) {
        $idCategorie = $categorie->getIdCategorie();
        $collectionProduit = array();
        $pdo = (new DBA())->getPDO();
        $pdoStatement = $pdo->prepare(Produit::$selectByIdCategorie);
        $pdoStatement->bindParam(":idCategorie", $idCategorie);
        $pdoStatement->execute();
        $recordSet = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
        foreach ($recordSet as $record) {
            $collectionProduit[] = Produit::arrayToProduit($record, $categorie);
        }
        return $collectionProduit;
    }


    public static function fetchAll() {
        $collectionProduit = null;
        $pdo = (new DBA())->getPDO();
        $pdoStatement = $pdo->query(Produit::$select);
        $recordSet = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
        foreach ($recordSet as $record) {
            $collectionProduit[] = Produit::arrayToProduit($record);
        }
        return $collectionProduit;
    }

    public static function fetch($idProduit) {
        $dba = new DBA();
        $pdo = $dba->getPDO();
        $pdoStatement = $pdo->prepare(Produit::$selectById);
        $pdoStatement->bindParam(":idProduit", $idProduit);
        $pdoStatement->execute();
        $record = $pdoStatement->fetch(PDO::FETCH_ASSOC);
        $produit = Produit::arrayToProduit($record);
        return $produit;
    }

    public function save() {
        if ($this->idProduit == null) {
            $this->insert();
        } else {
            $this->update();
        }
    }

    public function insert() {
        $pdo = (new DBA())->getPDO();
        $pdoStatement = $pdo->prepare(Produit::$insert);
        $pdoStatement->bindParam(":libelle", $this->libelle);
        $pdoStatement->bindParam(":description", $this->description);
        $pdoStatement->bindParam(":image", $this->image);
        $pdoStatement->bindParam(":prix", $this->prix);
        if ($this->categorie != null) {
            $idCategorie = $this->categorie->getIdCategorie();
        }
        $pdoStatement->bindParam(":idCategorie", $idCategorie);
        $pdoStatement->execute();
        $this->idProduit = $pdo->lastInsertId();
    }

    public function update() {
        $pdo = (new DBA())->getPDO();
        $pdoStatement = $pdo->prepare(Produit::$update);
        $pdoStatement->bindParam("idProduit", $this->idProduit);
        $pdoStatement->bindParam(":libelle", $this->libelle);
        $pdoStatement->bindParam(":description", $this->description);
        $pdoStatement->bindParam(":image", $this->image);
        $pdoStatement->bindParam(":prix", $this->prix);
        if ($this->categorie != null) {
            $idCategorie = $this->categorie->getIdCategorie();
        }
        $pdoStatement->bindParam(":idCategorie", $idCategorie);
        $pdoStatement->execute();
    }

    public function delete() {
        $dba = new DBA();
        $pdo = $dba->getPDO();
        $pdoStatement = $pdo->prepare(Produit::$delete);
        $pdoStatement->bindParam("idProduit", $this->idProduit);
        $resultat = $pdoStatement->execute();
        $nblignesAffectees = $pdoStatement->rowCount();
        if ($nblignesAffectees == 1) {
            $this->getCategorie()->removeProduit($this);
            $this->idProduit = null;
        }
        return $resultat;
    }
    
    public function compareTo(Produit $produit) {
        return $produit->idProduit == $this->idProduit;
    }


}
